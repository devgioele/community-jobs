# Community Jobs

Keep track of your work and let clients have an overview!

## User stories

### Case 1
The user creates a new community in which he works.
He sets his name for that community.
A new DB entry is created in his Google Drive cloud.
He shares access to that DB entry by sending a QR code to his clients.
Clients can join that community by scanning the QR code, which contains a link to the worker's DB entry.
Clients set their name for that community.
Clients can see in real time what work is waiting to be paid or is paid.
Work is measured with time or number of completed tasks.

### Case 2
The worker wants to get paid and sends a push notification to clients that joined the community.
The client clicks on the notification and gets an invoice, including ways in which he can pay the worker.

### Case 3
The worker recognizes that he has been paid by a client and marks for what work the client paid. The client has paid a fraction of the work, so the worker writes the amount fraction that has been paid. E.g. the worker worked for 4 hours and the client paid him 32$. The worker signs the first 3 hours as fully paid, while he writes for the 4th hours that only 2$/10$ have been paid so far.
The DB is updated.

## Contributing
Feel free to make merge requests, but try sticking to the original purpose of the app. If you have larger plans, discuss them in an issue before you start coding!

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
Development just started.

